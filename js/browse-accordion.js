(function ($) {
Drupal.behaviors.aluBrowseAccordion = {
  attach: function (context, settings) {
    $('.item-list .accordion-1-outer-wrapper > .use-ajax').each(function(index,element){
      if(!$(this).parents('li.link1').hasClass('open')){
      if($(this).siblings('.accordion-1-wrapper').has('div.load-content')){
        $(this).parents('li.link1').find('div.load-content').slideUp('slow');
      }else{
        $(this).parents('li.link1').find('div.load-content').hide();
      }
    }
  $(this).click(function(){
    //$(this).removeAttr("href");
    $(this).parents('li.link1').addClass('open').siblings().removeClass('open');
    var loadContent = $(this).parents('li.link1').find('div.load-content');
    loadContent.addClass('loaded').slideDown('slow');
    return false;
  });
  });
  $('li.open .accordion-1-outer-wrapper > a').click(function(){
    $('li.open').removeClass('open');
  })
  $('.accordion-2-outer-wrapper > .use-ajax').click(function(){
    $(this).parent().addClass('openItem');
    $(this).parent().siblings().removeClass('openItem');
    if(!$(this).parent().siblings().children('.use-ajax').hasClass('openItem')){
      //$(this).parent().siblings().children('.alu-accordion-2-wrapper').hide();
    }
    return false;
  })
}
};
}(jQuery));
